t=(100,78.8,90,76,65,34,2,99)
print ' Other Methods in tuples'
print '------------------------'

length=len(t)
maximum=max(t)
minimum=min(t)


print 'Length of the tuple is:',length
print 'Highest value in the tuple is:',maximum
print 'Lowest value in the tuple is:',minimum
print 'Sum of the values in the tuple is:',sum(t)


