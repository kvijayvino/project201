d={1:'vani',2:'karthi',100:'Anand'}

print 'Keys are'
print '-------'
print d.keys()

print 'Values are'
print '----------'
print d.values()


print 'Dictionary Methods'
print '------------------'

print 'Before pop the element'
print '---------------------'
print d

print 'After pop the element'
print '---------------------'
d.pop(2)
print d

print 'After pop item function'
print '-----------------------'
d.popitem()
print d

print 'Keys are'
print '-------'
print d.keys()

