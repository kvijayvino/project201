import datetime
print '\n\n\n strftime'
print '------------'

print "Current year:",datetime.date.today().strftime("%y")
print "Current Month:",datetime.date.today().strftime("%B")

print "Week number:",datetime.date.today().strftime("%W")
print "Weekday in number:",datetime.date.today().strftime("%w")

print "Day of the year:",datetime.date.today().strftime("%j")

print "Day of the week in text:",datetime.date.today().strftime("%A")

print "Day of the month:",datetime.date.today().strftime("%d")
print "\n\n\n"

